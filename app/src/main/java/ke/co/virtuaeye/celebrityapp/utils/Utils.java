package ke.co.virtuaeye.celebrityapp.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import ke.co.virtuaeye.celebrityapp.R;

/**
 * Created by Frederick on 8/24/2015.
 */
public class Utils {
    private static final String TAG = Utils.class.getSimpleName();
    private static final String ACCESS_TOKEN = "access_token";

    private static SharedPreferences prefs = null;


    private static synchronized SharedPreferences getPrefs(Context c) {
        if (prefs == null)
            prefs = c.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        return prefs;

    }

    public static synchronized void setAccessToken(Context c, String accessToken) {
        SharedPreferences.Editor editor = getPrefs(c).edit();
        editor.putString(ACCESS_TOKEN, accessToken);
        editor.commit();

    }

    public static synchronized String getAccessToken(Context c) {
        return c.getSharedPreferences(TAG, 0).getString(ACCESS_TOKEN, null);
    }


    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static boolean copyToClipboard(Context context, String text) {
        try {
            int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context
                        .getSystemService(Context.CLIPBOARD_SERVICE);
                clipboard.setText(text);
            } else {
                android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context
                        .getSystemService(Context.CLIPBOARD_SERVICE);
                android.content.ClipData clip = android.content.ClipData
                        .newPlainText(context.getString(R.string.copy_app_url_intro), text);
                clipboard.setPrimaryClip(clip);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }


}
