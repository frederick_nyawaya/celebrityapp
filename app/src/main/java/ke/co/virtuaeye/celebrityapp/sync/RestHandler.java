package ke.co.virtuaeye.celebrityapp.sync;

import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import ke.co.virtuaeye.celebrityapp.App;
import ke.co.virtuaeye.celebrityapp.Constants;

/**
 * Created by Frederick on 10/1/2015.
 */
public class RestHandler {
    private static final String TAG = RestHandler.class.getSimpleName();

    private static final String USER_AGENT = "";

    private GraphRequest.Callback callback;

    public GraphRequest.Callback getCallback() {
        return callback;
    }

    public void setCallback(GraphRequest.Callback callback) {
        this.callback = callback;
    }

    public JSONArray getYoutubeVideos() throws Exception {
        return new JSONObject(sendGet("", Constants.YOUTUBE_PL)).getJSONArray("items");
    }


    public GraphResponse getContent() {

        Bundle params = new Bundle();
        params.putString("fields", "message,story,created_time,picture,full_picture,name");
        return makeApiCall("/ActorMohanlal/feed", params);
    }

    public GraphResponse getProfilePic() {
        Bundle params = new Bundle();
        params.putString("fields", "picture");
        return makeApiCall("/ActorMohanlal", params);
    }

    private GraphResponse makeApiCall(String path, Bundle params) {
        FacebookSdk.sdkInitialize(App.context);
        AccessToken token = new AccessToken("442226802644021|5bb7802651f7f98cce4e5773e628d9b3",
                "442226802644021", "1709827384", null, null, null, null, null);
         /* make the API call */
        GraphRequest request = new GraphRequest(
                token,
                path,
                null,
                HttpMethod.GET,
                callback
        );
        request.setParameters(params);
        return request.executeAndWait();
    }


    private static synchronized String sendGet(String data, String url) throws Exception {
        URL obj = new URL(url);
        Log.i(TAG, "sending post - " + obj.toURI().toString() + " data - " + data);

        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

        // add reuqest header
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

//        // Send post request
//        con.setDoOutput(true);
//        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
//        wr.writeBytes(data);
//        wr.flush();
//        wr.close();

        int responseCode = con.getResponseCode();

        BufferedReader in = new BufferedReader(new InputStreamReader(
                con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        // print result
        Log.i("Moth", "responseCode - " + responseCode);
        Log.i("Moth", "response - " + response);
        if (responseCode == 200)
            return response.toString();
        else
            return null;

    }


}
