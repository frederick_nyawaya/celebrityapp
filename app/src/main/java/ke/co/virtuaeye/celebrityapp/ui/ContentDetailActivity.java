package ke.co.virtuaeye.celebrityapp.ui;

import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.poliveira.parallaxrecyclerview.ParallaxRecyclerAdapter;

import ke.co.virtuaeye.celebrityapp.R;
import ke.co.virtuaeye.celebrityapp.data.Contract;
import ke.co.virtuaeye.celebrityapp.sync.Backbone;
import ke.co.virtuaeye.celebrityapp.utils.DefaultImageLoader;

public class ContentDetailActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    ContentPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    public Toolbar toolbar;

    private static final String[] ARTICLE_COLUMNS = {
            Contract.ImageEntry.TABLE_NAME + "." + Contract.ImageEntry.C_URL,
            Contract.ArticleEntry.TABLE_NAME + "." + Contract.ArticleEntry.C_TITLE,
            Contract.ArticleEntry.TABLE_NAME + "." + Contract.ArticleEntry.C_TIME,
            Contract.ArticleEntry.TABLE_NAME + "." + Contract.ArticleEntry.C_ID,
            Contract.ArticleEntry.C_AUTHOR,
            Contract.ArticleEntry.C_CONTENT
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_detail);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new ContentPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setVisibility(View.GONE);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        getSupportLoaderManager().initLoader(0, null, this);



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri contentUri = Contract.ArticleEntry.CONTENT_URI;
        return new CursorLoader(
                this,
                contentUri,
                ARTICLE_COLUMNS,
                Contract.ArticleEntry.C_SOURCE + "='" + getIntent().getStringExtra(Contract.ArticleEntry.C_SOURCE) + "'",
                null,
                Contract.ArticleEntry.TABLE_NAME + "." + Contract.ArticleEntry.C_TIME + " DESC"
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mSectionsPagerAdapter.setData(data);
        mViewPager.setCurrentItem(getIntent().getIntExtra("position", 0));
        mViewPager.setVisibility(View.VISIBLE);

        Log.i("CDA", "loader finished ");

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class ContentPagerAdapter extends FragmentPagerAdapter {
        Cursor data;

        public ContentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public void setData(Cursor data) {
            this.data = data;
            this.notifyDataSetChanged();
        }

        @Override
        public Fragment getItem(int position) {
            data.moveToPosition(position);
            return ContentDetailFragment.newInstance(data);
        }

        @Override
        public int getCount() {
            return data != null ? data.getCount() : 0;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            data.moveToPosition(position);
            return data.getString(1);
        }
    }


    public static class ContentDetailFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static ContentDetailFragment newInstance(Cursor data) {
            ContentDetailFragment fragment = new ContentDetailFragment();
            Bundle args = new Bundle();
            args.putString(Contract.ArticleEntry.C_TITLE, data.getString(1));
            args.putString(Contract.ArticleEntry.C_DESCRIPTION, data.getString(5));
            args.putString(Contract.ImageEntry.C_URL, data.getString(0));
            args.putLong(Contract.ArticleEntry.C_TIME, data.getLong(2));
            fragment.setArguments(args);
            return fragment;
        }

        public ContentDetailFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.recycler, container, false);
            RecyclerView recycler = (RecyclerView) rootView.findViewById(R.id.recycler);


            ParallaxRecyclerAdapter<String> adapter = new ParallaxRecyclerAdapter<String>(null) {
                @Override
                public void onBindViewHolderImpl(RecyclerView.ViewHolder viewHolder, ParallaxRecyclerAdapter<String> adapter, int i) {

                    ((MyCustomViewHolder) viewHolder).content.setText(
                            Html.fromHtml(Backbone.getParagraphs(getArguments().getString(Contract.ArticleEntry.C_DESCRIPTION))));
                }

                @Override
                public RecyclerView.ViewHolder onCreateViewHolderImpl(ViewGroup viewGroup, final ParallaxRecyclerAdapter<String> adapter, int i) {
                    // Here is where you inflate your row and pass it to the constructor of your ViewHolder
                    return new MyCustomViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_content_detail, viewGroup, false));
                }

                @Override
                public int getItemCountImpl(ParallaxRecyclerAdapter<String> adapter) {
                    // return the content of your array
                    return 1;
                }


            };

            recycler.setLayoutManager(new GridLayoutManager(getActivity(), 1));

            View header = LayoutInflater.from(getActivity()).inflate(
                    R.layout.header_content, null, false);

            ImageView headerImage = (ImageView) header.findViewById(R.id.imageView2);
            TextView headerTitle = (TextView) header.findViewById(R.id.textView5);
            TextView headerTime = (TextView) header.findViewById(R.id.textView8);

            DefaultImageLoader.getInstance().loadImage(getArguments().getString(Contract.ImageEntry.C_URL), headerImage);
            headerTitle.setText(getArguments().getString(Contract.ArticleEntry.C_TITLE));
            headerTime.setText(DateUtils.getRelativeTimeSpanString(getArguments().getLong(Contract.ArticleEntry.C_TIME)));

            headerTitle.setVisibility(View.VISIBLE);


            adapter.setParallaxHeader(header, recycler);

            adapter.setOnParallaxScroll(new ParallaxRecyclerAdapter.OnParallaxScroll() {
                @Override
                public void onParallaxScroll(float percentage, float offset, View paralaxx) {
                    Drawable c = ((ContentDetailActivity) getActivity()).toolbar.getBackground();
                    c.setAlpha(Math.round(percentage * 255));
                    ((ContentDetailActivity) getActivity()).toolbar.setBackground(c);
                }
            });

            recycler.setAdapter(adapter);

            return rootView;
        }


        private class MyCustomViewHolder extends RecyclerView.ViewHolder {
            public TextView content;

            public MyCustomViewHolder(View parent) {
                super(parent);
                content = (TextView) parent.findViewById(R.id.textView7);

            }
        }

    }
}


