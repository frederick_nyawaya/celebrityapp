package ke.co.virtuaeye.celebrityapp.sync;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import com.google.code.rome.android.repackaged.com.sun.syndication.feed.synd.SyndFeed;
import com.google.code.rome.android.repackaged.com.sun.syndication.fetcher.FeedFetcher;
import com.google.code.rome.android.repackaged.com.sun.syndication.fetcher.FetcherException;
import com.google.code.rome.android.repackaged.com.sun.syndication.fetcher.impl.HttpURLFeedFetcher;
import com.google.code.rome.android.repackaged.com.sun.syndication.io.FeedException;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import ke.co.virtuaeye.celebrityapp.data.Contract;

/**
 * Created by Frederick on 9/3/2015.
 */
public class Sync {
    public static final String TAG = Sync.class.getSimpleName();
    ContentResolver contentResolver;
    Context context;

    public Sync(Context context) {
        this.context = context;
        contentResolver = context.getContentResolver();

    }

    public void sync() throws IOException, FetcherException, FeedException {
        for (String source : getSourceUrls()) {
            int exceptionCount = 0;
            SyndFeed feed = retrieveFeed(source);
            try {
                ContentValues[] articles = Backbone.getArticles(feed, source);
                int num = 0;
                if (articles != null)
                    num = contentResolver.bulkInsert(Contract.ArticleEntry.CONTENT_URI, articles);
                Log.i(TAG, "inserted articles - " + num);

                processImages(articles);

            } catch (IOException e) {
                e.printStackTrace();
                exceptionCount++;
            }

        }
    }


    private void processImages(ContentValues[] articles) throws IOException {
        for (int i = 0; i < articles.length; i++) {
            ContentValues val = articles[i];
            contentResolver.bulkInsert(Contract.ImageEntry.CONTENT_URI,
                    Backbone.getImages(val.getAsString(Contract.ArticleEntry.C_CONTENT), val.getAsString(Contract.ArticleEntry.C_ID)));

        }
    }

    //from web, file, yada yada
    private List<String> getSourceUrls() throws IOException {
        Scanner scan = new Scanner(context.getAssets().open("sources"));
        List<String> urls = new ArrayList<>();

        while (scan.hasNext()) {
            urls.add(scan.nextLine());
        }
        return urls;
    }

    /**
     * Instatiates a new HttpURLFeedFetcher object, sets the required user agent
     * and retrieves the feeds
     *
     * @param feedUrl
     * @return SyndFeed object
     * @throws IOException
     * @throws FeedException
     * @throws FetcherException
     */
    private static SyndFeed retrieveFeed(final String feedUrl)
            throws IOException, FeedException, FetcherException {
        FeedFetcher feedFetcher = new HttpURLFeedFetcher();
        feedFetcher
                .setUserAgent("Mozilla/5.0 (X11; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/12.0");
        return feedFetcher.retrieveFeed(new URL(feedUrl));
    }

}
