package ke.co.virtuaeye.celebrityapp.ui;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import ke.co.virtuaeye.celebrityapp.Constants;
import ke.co.virtuaeye.celebrityapp.R;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class YoutubePlayerActivity extends YouTubeFailureRecoveryActivity implements YouTubePlayer.OnInitializedListener {

    private boolean isFullscreen;
    YouTubePlayerView youTubeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_player);

        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        youTubeView.initialize(Constants.DEVELOPER_KEY, this);

        layout();
        checkYouTubeApi();
    }

    private void checkYouTubeApi() {
        YouTubeInitializationResult errorReason =
                YouTubeApiServiceUtil.isYouTubeApiServiceAvailable(this);
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, 0).show();
        } else if (errorReason != YouTubeInitializationResult.SUCCESS) {
            String errorMessage =
                    String.format(getString(R.string.error_player), errorReason.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }
    // Utility methods for layouting.

    private int dpToPx(int dp) {
        return (int) (dp * getResources().getDisplayMetrics().density + 0.5f);
    }

    private static void setLayoutSize(View view, int width, int height) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = width;
        params.height = height;
        view.setLayoutParams(params);
    }

    private static void setLayoutSizeAndGravity(View view, int width, int height, int gravity) {
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
        params.width = width;
        params.height = height;
        params.gravity = gravity;
        view.setLayoutParams(params);
    }


    private void layout() {
        if (isFullscreen) {
            youTubeView.setTranslationY(0); // Reset any translation that was applied in portrait.
            //setLayoutSize(videoFragment.getView(), MATCH_PARENT, MATCH_PARENT);
            setLayoutSizeAndGravity(youTubeView, MATCH_PARENT, MATCH_PARENT, Gravity.TOP | Gravity.LEFT);
        }
    }


        @Override
        public void onInitializationSuccess (YouTubePlayer.Provider provider, YouTubePlayer
        youTubePlayer,boolean wasRestored){
//        this.player = player;
//
            youTubePlayer.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
//        player.setOnFullscreenListener((VideoListDemoActivity) getActivity());
//        if (!restored && videoId != null) {
//            player.cueVideo(videoId);
//        }

            if (!wasRestored) {
                youTubePlayer.cueVideo(getIntent().getStringExtra("video_id"));
                //youTubePlayer.cueVideo("1KhZKNZO8mQ");
                //youTubePlayer.cuePlaylist("1KhZKNZO8mQ");
                //  youTubePlayer.setFullscreen(true);
            }

        }

        @Override
        public void onInitializationFailure (YouTubePlayer.Provider
        provider, YouTubeInitializationResult youTubeInitializationResult){

        }

        @Override
        protected YouTubePlayer.Provider getYouTubePlayerProvider () {
            return null;
        }
    }
