package ke.co.virtuaeye.celebrityapp.sync;

import android.content.ContentValues;
import android.util.Log;

import com.google.code.rome.android.repackaged.com.sun.syndication.feed.synd.SyndEntry;
import com.google.code.rome.android.repackaged.com.sun.syndication.feed.synd.SyndFeed;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ke.co.virtuaeye.celebrityapp.data.Contract;

/**
 * Created by Frederick on 9/3/2015.
 */
public class Backbone {

    public static ContentValues[] getImages(String html, String ownerId) throws IOException {
        List<String> images = getImgLinks(html);
        if (images != null) {
            ContentValues[] values = new ContentValues[images.size()];
            for (String image : images)
                values[images.indexOf(image)] = getImage(image, ownerId);
            return values;
        }
        return null;

    }


    public static ContentValues[] getArticles(SyndFeed feed, String ownerId) throws IOException {
        if (feed != null) {
            List<SyndEntry> entries = feed.getEntries();
            ContentValues[] listings = new ContentValues[entries.size()];
            for (SyndEntry entry : entries)
                listings[feed.getEntries().indexOf(entry)] = getArticle(entry, ownerId);
            return listings;
        }
        return null;

    }

    private static ContentValues getArticle(SyndEntry syndEntry, String sourceId) {
        ContentValues values = new ContentValues();
        values.put(Contract.ArticleEntry.C_TITLE, syndEntry.getTitle());
        values.put(Contract.ArticleEntry.C_URL, syndEntry.getLink());
        values.put(Contract.ArticleEntry.C_AUTHOR, syndEntry.getAuthor());
        values.put(Contract.ArticleEntry.C_CONTENT, syndEntry.getContents().get(0).toString()
                .replace("SyndContentImpl.value=", "")
                .replace("SyndContentImpl.type=html", "")
                .replace("SyndContentImpl.interface=interface", "")
                .replace(
                        "com.google.code.rome.android.repackaged.com.sun.syndication.feed.synd.SyndContent",
                        "")
                .replace("SyndContentImpl.mode=null", ""));
        values.put(Contract.ArticleEntry.C_DESCRIPTION, syndEntry.getDescription().getValue());
        values.put(Contract.ArticleEntry.C_SOURCE, sourceId);
        values.put(Contract.ArticleEntry.C_ID, syndEntry.getUri());
        values.put(Contract.ArticleEntry.C_TIME, syndEntry.getPublishedDate() != null ? syndEntry.getPublishedDate()
                .getTime() : System.currentTimeMillis());

        Log.i("meh", values.getAsString(Contract.ArticleEntry.C_CONTENT));
        return values;
    }

    private static ContentValues getSource(SyndFeed source) {
        ContentValues values = new ContentValues();
        values.put(Contract.SourceEntry.C_TITLE, source.getTitle());
        values.put(Contract.SourceEntry.C_URL, source.getUri());
        return values;
    }

    private static ContentValues getImage(String link, String ownerId) {
        ContentValues values = new ContentValues();
        values.put(Contract.ImageEntry.C_OWNER_ID, ownerId);
        values.put(Contract.ImageEntry.C_URL, link);
        values.put(Contract.ImageEntry.C_ID, link + ownerId);
        return values;
    }

    private static List<String> getImgLinks(String html) {

        List<String> links = new ArrayList<>();
        Document dom = Jsoup.parse(html);
        Elements urls = dom.getElementsByTag("img");

        for (Element url : urls) {
            links.add(url.attr("src"));

        }
        return links;
    }

    public static String getParagraphs(String contentEncoded) {
        String paragraphs = "";
        if (contentEncoded == null || contentEncoded == "") {
            return null;
        } else {

            try {
                Document dom = Jsoup.parse(contentEncoded);
                Elements paragraphs_raw = dom.getElementsByTag("p");
                paragraphs_raw.size();
                int i = 0;
                for (Element paragraph : paragraphs_raw) {
                    i++;
                    String text = paragraph.text();
                    String p_class = paragraph.className();
                    int len = text.length();

                    if (!p_class.equals("wp-caption-text")) {

                        if (len > 1 && paragraphs_raw.size() != i) {
                            String line = "<br><br>";
                            paragraphs += text;
                            paragraphs += line;

                        }
                    }
                }
            } catch (Exception e) {
            }
            if (paragraphs == "") {
                try {
                    return Jsoup.clean(contentEncoded, Whitelist.simpleText());
                } catch (Exception e) {
                }
            }
            return paragraphs;
        }

    }

    public String getSimpleText(String html) {
        String simpletxt = html.replaceAll("</b>", "");
        return simpletxt.replaceAll("<b>", "");
    }
}
