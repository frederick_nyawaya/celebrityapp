package ke.co.virtuaeye.celebrityapp.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Frederick on 4/23/2015.
 */
public class DbHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 2;

    public static final String DATABASE_NAME = "mohanlal4.db";

    // Create a table to hold sources.
    final String SQL_CREATE_SOURCES_TABLE = "CREATE TABLE " + Contract.SourceEntry.TABLE_NAME + " (" +
            Contract.SourceEntry.C_URL + " TEXT PRIMARY KEY," +
            Contract.SourceEntry.C_TITLE + " TEXT);";

    // Create a table to hold articles.
    final String SQL_CREATE_ARTICLES_TABLE = "CREATE TABLE " + Contract.ArticleEntry.TABLE_NAME + " (" +
            Contract.ArticleEntry.C_ID + " TEXT PRIMARY KEY, " +
            Contract.ArticleEntry.C_TITLE + " TEXT, " +
            Contract.ArticleEntry.C_DESCRIPTION + " TEXT, " +
            Contract.ArticleEntry.C_CONTENT + " TEXT, " +
            Contract.ArticleEntry.C_AUTHOR + " TEXT, " +
            Contract.ArticleEntry.C_SOURCE + " TEXT, " +
            Contract.ArticleEntry.C_TIME + " INTEGER, " +
            Contract.ArticleEntry.C_URL + " TEXT);";

    // Create a table to hold images.
    final String SQL_CREATE_IMAGES_TABLE = "CREATE TABLE " + Contract.ImageEntry.TABLE_NAME + " (" +
            Contract.ImageEntry.C_ID + " TEXT PRIMARY KEY, " +
            Contract.ImageEntry.C_URL + " TEXT, " +
            Contract.ImageEntry.C_OWNER_ID + " TEXT);";


    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {


        db.execSQL(SQL_CREATE_ARTICLES_TABLE);
        db.execSQL(SQL_CREATE_SOURCES_TABLE);
        db.execSQL(SQL_CREATE_IMAGES_TABLE);

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
// This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        // Note that this only fires if you change the version number for your database.
        // It does NOT depend on the version number for your application.
        // If you want to update the schema without wiping data, commenting out the next 2 lines
        // should be your top priority before modifying this method.
//        db.execSQL("DROP TABLE IF EXISTS " + Contract.AgentEntry.TABLE_NAME);
//        db.execSQL("DROP TABLE IF EXISTS " + Contract.SourceEntry.TABLE_NAME);
//        onCreate(db);

        if (newVersion == 2) ;
        //db.execSQL(SQL_CREATE_IMAGES_TABLE);


    }

}



