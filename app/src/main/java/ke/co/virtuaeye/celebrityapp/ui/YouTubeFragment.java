package ke.co.virtuaeye.celebrityapp.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ke.co.virtuaeye.celebrityapp.Constants;
import ke.co.virtuaeye.celebrityapp.R;
import ke.co.virtuaeye.celebrityapp.sync.RestHandler;

/**
 * Created by Frederick on 8/24/2015.
 */
public class YouTubeFragment extends Fragment implements LoaderManager.LoaderCallbacks<JSONArray> {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_TITLE = "title";
    private ListView list;
    private VideoAdapter adapter;

    private RestHandler handler;
    private boolean dataReady = false;
    private JSONArray data;

    private ProgressBar progressBar;

    public YouTubeFragment() {
    }

    public static Fragment newInstance(int sectionNumber, String title) {
        Fragment fragment = new YouTubeFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putString(ARG_TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View parent = inflater.inflate(R.layout.list, container, false);
        list = (ListView) parent.findViewById(R.id.list);
        progressBar = (ProgressBar) parent.findViewById(R.id.progressBar);

        adapter = new VideoAdapter(getActivity(), null);
        list.setAdapter(adapter);

        handler = new RestHandler();

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), YoutubePlayerActivity.class);
                intent.putExtra("video_id", adapter.getItem(position).videoId);
                getActivity().startActivity(intent);

            }
        });


//
//
//        ParallaxRecyclerAdapter<String> adapter = new ParallaxRecyclerAdapter<VideoEntry>(myContent) {
//            @Override
//            public void onBindViewHolderImpl(RecyclerView.ViewHolder viewHolder, ParallaxRecyclerAdapter<String> adapter, int i) {
//                // If you're using your custom handler (as you should of course)
//                // you need to cast viewHolder to it.
//                Log.i("WAAT", "co - " + myContent.get(i));
//                ((MyCustomViewHolder) viewHolder).title.setText(myContent.get(i).title); // your bind holder routine.
//               // DefaultImageLoader.getInstance().loadImage("drawable://" + pics.get(i), ((MyCustomViewHolder) viewHolder).image);
//            }
//
//            @Override
//            public RecyclerView.ViewHolder onCreateViewHolderImpl(ViewGroup viewGroup, final ParallaxRecyclerAdapter<String> adapter, int i) {
//                // Here is where you inflate your row and pass it to the constructor of your ViewHolder
//                return new MyCustomViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_video_mod, viewGroup, false));
//            }
//
//            @Override
//            public int getItemCountImpl(ParallaxRecyclerAdapter<String> adapter) {
//                // return the content of your array
//                return myContent.size();
//            }
//
//
//        };
//
//        recycler.setLayoutManager(new GridLayoutManager(getActivity(), 1));
//
//        View header = LayoutInflater.from(getActivity()).inflate(
//                R.layout.header_content, null, false);
//        ImageView bg = (ImageView) header.findViewById(R.id.imageView2);
//        DefaultImageLoader.getInstance().loadImage("drawable://" + R.drawable.bg_a, bg);
//        adapter.setParallaxHeader(header, recycler);
//
//        adapter.setOnParallaxScroll(new ParallaxRecyclerAdapter.OnParallaxScroll() {
//            @Override
//            public void onParallaxScroll(float percentage, float offset, View paralaxx) {
//                Drawable c = ((HomeActivity) getActivity()).toolbar.getBackground();
//                c.setAlpha(Math.round(percentage * 255));
//                ((HomeActivity) getActivity()).toolbar.setBackground(c);
//            }
//        });
//
//        adapter.setOnClickEvent(new ParallaxRecyclerAdapter.OnClickEvent() {
//            @Override
//            public void onClick(View view, int i) {
//                getActivity().startActivity(new Intent(getActivity(), ContentDetailActivity.class));
//                Log.i("ME", "cicked - " + i);
//            }
//        });
//
//
//        recycler.setAdapter(adapter);

        return parent;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(1, null, this);
    }

    @Override
    public Loader<JSONArray> onCreateLoader(int id, Bundle args) {
        progressBar.setVisibility(View.VISIBLE);
        list.setVisibility(View.GONE);
        return new AsyncTaskLoader<JSONArray>(getActivity()) {
            @Override
            protected void onStartLoading() {
                super.onStartLoading();
                if (dataReady) {
                    deliverResult(data);
                } else {
                    forceLoad();
                }
            }

            @Override
            public JSONArray loadInBackground() {
                try {
                    return handler.getYoutubeVideos();
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

    }

    @Override
    public void onLoadFinished(Loader<JSONArray> loader, JSONArray data) {
        progressBar.setVisibility(View.GONE);
        list.setVisibility(View.VISIBLE);
        try {
            adapter.setEntries(getYoutubeVideos(data));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onLoaderReset(Loader<JSONArray> loader) {

    }

    public List<YouTubeFragment.VideoEntry> getYoutubeVideos(JSONArray items) throws Exception {

        List<YouTubeFragment.VideoEntry> itemsList = new ArrayList<>();

        for (int i = 0; i < items.length(); i++) {
            itemsList.add(getVideoEntry(items.getJSONObject(i)));
        }

        return itemsList;
    }

    private YouTubeFragment.VideoEntry getVideoEntry(JSONObject data) throws JSONException {
        return new YouTubeFragment.VideoEntry(data.getJSONObject("snippet").getString("title"), data.getJSONObject("contentDetails").getString("videoId"));
    }

    public static final class VideoFragment extends YouTubePlayerFragment
            implements YouTubePlayer.OnInitializedListener {

        private YouTubePlayer player;
        private String videoId;

        public static VideoFragment newInstance() {
            return new VideoFragment();
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            initialize(Constants.DEVELOPER_KEY, this);
        }

        @Override
        public void onDestroy() {
            if (player != null) {
                player.release();
            }
            super.onDestroy();
        }

        public void setVideoId(String videoId) {
            if (videoId != null && !videoId.equals(this.videoId)) {
                this.videoId = videoId;
                if (player != null) {
                    player.cueVideo(videoId);
                }
            }
        }

        public void pause() {
            if (player != null) {
                player.pause();
            }
        }

        @Override
        public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean restored) {
            this.player = player;

            player.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
            player.setOnFullscreenListener((HomeActivity) getActivity());
            if (!restored && videoId != null) {
                player.cueVideo(videoId);
            }
        }

        @Override
        public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult result) {
            this.player = null;
        }

    }

    private class VideoAdapter extends BaseAdapter {
        private List<VideoEntry> entries;
        private final List<View> entryViews;
        private final Map<YouTubeThumbnailView, YouTubeThumbnailLoader> thumbnailViewToLoaderMap;
        private final LayoutInflater inflater;
        private final ThumbnailListener thumbnailListener;


        private boolean labelsVisible;

        public VideoAdapter(Context context, List<VideoEntry> entries) {
            this.entries = entries;

            entryViews = new ArrayList<View>();
            thumbnailViewToLoaderMap = new HashMap<YouTubeThumbnailView, YouTubeThumbnailLoader>();
            inflater = LayoutInflater.from(context);
            thumbnailListener = new ThumbnailListener();

            labelsVisible = true;
        }

        public void setEntries(List<VideoEntry> entries) {
            this.entries = entries;
            this.notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return entries != null ? entries.size() : 0;
        }

        @Override
        public VideoEntry getItem(int position) {
            return entries.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            VideoEntry entry = entries.get(position);

            // There are three cases here
            if (view == null) {
                // 1) The view has not yet been created - we need to initialize the YouTubeThumbnailView.
                view = inflater.inflate(R.layout.list_item_video_mod, parent, false);
                YouTubeThumbnailView thumbnail = (YouTubeThumbnailView) view.findViewById(R.id.thumbnail);
                thumbnail.setTag(entry.videoId);
                thumbnail.initialize(Constants.DEVELOPER_KEY, thumbnailListener);
            } else {
                YouTubeThumbnailView thumbnail = (YouTubeThumbnailView) view.findViewById(R.id.thumbnail);
                YouTubeThumbnailLoader loader = thumbnailViewToLoaderMap.get(thumbnail);
                if (loader == null) {
                    // 2) The view is already created, and is currently being initialized. We store the
                    //    current videoId in the tag.
                    thumbnail.setTag(entry.videoId);
                } else {
                    // 3) The view is already created and already initialized. Simply set the right videoId
                    //    on the loader.
                    thumbnail.setImageResource(R.drawable.loading_thumbnail);
                    loader.setVideo(entry.videoId);
                }
            }
            TextView label = ((TextView) view.findViewById(R.id.textView));
            label.setText(entry.title);
            label.setVisibility(labelsVisible ? View.VISIBLE : View.GONE);
            return view;
        }

        private final class ThumbnailListener implements
                YouTubeThumbnailView.OnInitializedListener,
                YouTubeThumbnailLoader.OnThumbnailLoadedListener {

            @Override
            public void onInitializationSuccess(
                    YouTubeThumbnailView view, YouTubeThumbnailLoader loader) {
                loader.setOnThumbnailLoadedListener(this);
                thumbnailViewToLoaderMap.put(view, loader);
                view.setImageResource(R.drawable.loading_thumbnail);
                String videoId = (String) view.getTag();
                loader.setVideo(videoId);
            }

            @Override
            public void onInitializationFailure(
                    YouTubeThumbnailView view, YouTubeInitializationResult loader) {
                view.setImageResource(R.drawable.no_thumbnail);
            }

            @Override
            public void onThumbnailLoaded(YouTubeThumbnailView view, String videoId) {
            }

            @Override
            public void onThumbnailError(YouTubeThumbnailView view, YouTubeThumbnailLoader.ErrorReason errorReason) {
                view.setImageResource(R.drawable.no_thumbnail);
            }
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((HomeActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER), getArguments().getString(ARG_TITLE));
    }

    private class MyCustomViewHolder extends RecyclerView.ViewHolder {
        public TextView title, time;
        public YouTubeThumbnailView thumbnailView;

        public MyCustomViewHolder(View parent) {
            super(parent);
            title = (TextView) parent.findViewById(R.id.textView);
            thumbnailView = (YouTubeThumbnailView) parent.findViewById(R.id.thumbnail);


        }
    }

    public static final class VideoEntry {
        private final String title;
        private final String videoId;
        private String time, description;

        public VideoEntry(String title, String videoId) {
            this.title = title;
            this.videoId = videoId;
        }
    }
}
