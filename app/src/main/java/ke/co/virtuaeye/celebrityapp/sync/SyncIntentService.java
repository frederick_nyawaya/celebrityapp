package ke.co.virtuaeye.celebrityapp.sync;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

import com.google.code.rome.android.repackaged.com.sun.syndication.fetcher.FetcherException;
import com.google.code.rome.android.repackaged.com.sun.syndication.io.FeedException;

import java.io.IOException;


public class SyncIntentService extends IntentService {

    public static void startSync(Context context) {
        Intent intent = new Intent(context, SyncIntentService.class);
        context.startService(intent);
    }

    public SyncIntentService() {
        super("SyncIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            try {
                new Sync(this).sync();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (FetcherException e) {
                e.printStackTrace();
            } catch (FeedException e) {
                e.printStackTrace();
            }
        }
    }

}