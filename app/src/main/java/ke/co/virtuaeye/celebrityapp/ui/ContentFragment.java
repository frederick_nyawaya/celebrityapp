package ke.co.virtuaeye.celebrityapp.ui;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.poliveira.parallaxrecyclerview.ParallaxRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ke.co.virtuaeye.celebrityapp.R;
import ke.co.virtuaeye.celebrityapp.data.Contract;
import ke.co.virtuaeye.celebrityapp.sync.Backbone;
import ke.co.virtuaeye.celebrityapp.utils.DefaultImageLoader;

/**
 * Created by Frederick on 8/24/2015.
 */
public class ContentFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_TITLE = "title";

    private static String[] contentUrls = {
            "http://mohanlal.ideations.in/category/about/feed",
            "http://mohanlal.ideations.in/category/new-releases/feed",
            "http://mohanlal.ideations.in/category/upcoming-movies/feed",
            "http://mohanlal.ideations.in/category/filmography/feed"
    };


    private static final String[] ARTICLE_COLUMNS = {
            Contract.ImageEntry.TABLE_NAME + "." + Contract.ImageEntry.C_URL,
            Contract.ArticleEntry.TABLE_NAME + "." + Contract.ArticleEntry.C_TITLE,
            Contract.ArticleEntry.TABLE_NAME + "." + Contract.ArticleEntry.C_TIME,
            Contract.ArticleEntry.TABLE_NAME + "." + Contract.ArticleEntry.C_ID,
            Contract.ArticleEntry.C_AUTHOR,
            Contract.ArticleEntry.C_CONTENT
    };

    ContentAdapter adapter;
    ImageView headerImage;
    TextView headerTitle, headerTime;


    public ContentFragment() {
    }


    public static Fragment newInstance(int sectionNumber, String title) {
        Fragment fragment = new ContentFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putString(ARG_TITLE, title);

        args.putString(Contract.SourceEntry.C_URL, contentUrls[sectionNumber - 1]);
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.recycler, container, false);
        RecyclerView recycler = (RecyclerView) rootView.findViewById(R.id.recycler);
        recycler.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        View header = LayoutInflater.from(getActivity()).inflate(
                R.layout.header_content, null, false);
        headerImage = (ImageView) header.findViewById(R.id.imageView2);
        headerTitle = (TextView) header.findViewById(R.id.textView5);
        headerTime = (TextView) header.findViewById(R.id.textView8);

        adapter = new ContentAdapter(null);
        adapter.setParallaxHeader(header, recycler);

        adapter.setOnParallaxScroll(new ParallaxRecyclerAdapter.OnParallaxScroll() {
            @Override
            public void onParallaxScroll(float percentage, float offset, View paralaxx) {
                Drawable c = ((HomeActivity) getActivity()).toolbar.getBackground();
                c.setAlpha(Math.round(percentage * 255));
                ((HomeActivity) getActivity()).toolbar.setBackground(c);
            }
        });

//        adapter.setOnClickEvent(new ParallaxRecyclerAdapter.OnClickEvent() {
//            @Override
//            public void onClick(View view, int i) {
//                openDetailActivity(i + 1);
//            }
//        });


        recycler.setAdapter(adapter);

        return rootView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        getLoaderManager().initLoader(0, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((HomeActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER), getArguments().getString(ARG_TITLE));
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri contentUri = Contract.ArticleEntry.CONTENT_URI;
        return new CursorLoader(
                getActivity(),
                contentUri,
                ARTICLE_COLUMNS, Contract.ArticleEntry.C_SOURCE + "='" + getArguments().getString(Contract.SourceEntry.C_URL) + "'",
                null,
                Contract.ArticleEntry.TABLE_NAME + "." + Contract.ArticleEntry.C_TIME + " DESC"
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.setData(data);
        Log.i("CF", "loader finished ");

        //to get a random image/article from last pos moving up, exclude first to third?

        List<Integer> candidates = new ArrayList<>();

        while (data.moveToNext()) {
            String imageUrl = data.getString(0);
            if (imageUrl != null && imageUrl.length() > 0)
                candidates.add(data.getPosition());
        }

        int selected = 0;
        try {
            selected = candidates.get(new Random().nextInt(candidates.size() - 2) + 2);//excludes the first two candidates
        } catch (Exception e) {
            e.printStackTrace();
        }
        data.moveToPosition(selected);
        setUpHeader(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private void setUpHeader(Cursor c) {
        DefaultImageLoader.getInstance().loadImage(c.getString(0), headerImage);
        headerTitle.setText(c.getString(1));
        headerTime.setText(DateUtils.getRelativeTimeSpanString(c.getLong(2)));

    }

    private void openDetailActivity(int position) {
        Intent intent = new Intent(getActivity(), ContentDetailActivity.class);
        intent.putExtra(Contract.ArticleEntry.C_SOURCE, getArguments().getString(Contract.SourceEntry.C_URL));
        intent.putExtra("position", position);
        getActivity().startActivity(intent);
    }

    private class ContentAdapter extends ParallaxRecyclerAdapter<Cursor> {
        Cursor data;

        public ContentAdapter(List<Cursor> data) {
            super(data);

        }

        public void setData(Cursor data) {
            this.data = data;
            this.notifyDataSetChanged();
        }

        @Override
        public void onBindViewHolderImpl(RecyclerView.ViewHolder viewHolder, ParallaxRecyclerAdapter<Cursor> parallaxRecyclerAdapter, final int i) {
            data.moveToPosition(i);
            ((MyCustomViewHolder) viewHolder).title.setText(data.getString(1));
            ((MyCustomViewHolder) viewHolder).description.setText(Html.fromHtml(Backbone.getParagraphs(data.getString(5).trim())));
            ((MyCustomViewHolder) viewHolder).time.setText(DateUtils.getRelativeTimeSpanString(data.getLong(2)));

            String url = data.getString(0);
            if (url != null && url.length() > 0) {
                ((MyCustomViewHolder) viewHolder).image.setVisibility(View.VISIBLE);
                DefaultImageLoader.getInstance().loadImage(data.getString(0), ((MyCustomViewHolder) viewHolder).image);
            } else {
                ((MyCustomViewHolder) viewHolder).image.setVisibility(View.GONE);
            }

            ((MyCustomViewHolder) viewHolder).mParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openDetailActivity(i);
                    Log.i("clciked", "here - " + i);
                }
            });


        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolderImpl(ViewGroup viewGroup, ParallaxRecyclerAdapter<Cursor> parallaxRecyclerAdapter, int i) {
            return new MyCustomViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_card_mod, viewGroup, false));
        }

        @Override
        public int getItemCountImpl(ParallaxRecyclerAdapter<Cursor> parallaxRecyclerAdapter) {
            return data != null ? data.getCount() : 0;
        }
    }

    private class MyCustomViewHolder extends RecyclerView.ViewHolder {
        public TextView title, description, time;
        public ImageView image;
        public View mParent;

        public MyCustomViewHolder(View parent) {
            super(parent);
            time = (TextView) parent.findViewById(R.id.textView3);
            title = (TextView) parent.findViewById(R.id.textView);
            description = (TextView) parent.findViewById(R.id.textView2);
            image = (ImageView) parent.findViewById(R.id.imageView5);
            mParent = parent;
            mParent.setClickable(true);

        }
    }
}
