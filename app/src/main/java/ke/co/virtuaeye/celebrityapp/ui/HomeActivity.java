package ke.co.virtuaeye.celebrityapp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubePlayer;

import ke.co.virtuaeye.celebrityapp.R;
import ke.co.virtuaeye.celebrityapp.sync.SyncIntentService;
import ke.co.virtuaeye.celebrityapp.utils.Utils;

public class HomeActivity extends AppCompatActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, YouTubePlayer.OnFullscreenListener {

    private Spinner spinner;
    ArrayAdapter<String> adapterAbt, adapterVid;

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    public Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        spinner = (Spinner) findViewById(R.id.spinner);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));


        adapterVid = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_activated_1,
                android.R.id.text1,
                new String[]{
                        "Comedy",
                        "Action",
                        "Songs",
                        "Master of the art"
                });

        SyncIntentService.startSync(this);

    }


    @Override
    public void onNavigationDrawerItemSelected(int position, String title) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment;
        try {
            spinner.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        switch (position) {
            case 0:
                fragment = PlaceholderFragment.newInstance(PlaceholderFragment.ABOUT);
                onSectionAttached(position, "MOHANLAL");
                break;
            case 5:
                //set up spinner
                spinner.setAdapter(adapterVid);
                spinner.setVisibility(View.VISIBLE);
                fragment = YouTubeFragment.newInstance(position, title);
                SyncIntentService.startSync(this);
                break;
            case 6:
                fragment = FacebookFragment.newInstance();
                onSectionAttached(position, "Facebook");

                break;
            case 7:
                shareApp();
                return;
            case 8:
                fragment = PlaceholderFragment.newInstance(PlaceholderFragment.FEEDBACK);
                break;
            case 9:
                fragment = PlaceholderFragment.newInstance(PlaceholderFragment.CONTACT_US);
                break;
            case 10:
                fragment = PlaceholderFragment.newInstance(PlaceholderFragment.HELP);
                break;
            default:
                fragment = ContentFragment.newInstance(position, title);

        }

        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    public void onSectionAttached(int number, String title) {
        if (number == 0)
            mTitle = getString(R.string.app_name);
        else if (number == 1)
            mTitle = "About";
        else
            mTitle = title;
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    public void shareApp() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Download the MOHANLAL App - https://play.google.com/store/apps/details?id=package-id\"");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.home, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        } else if (id == R.id.action_contact_us) {
            onNavigationDrawerItemSelected(8, "Contact Us");
        } else if (id == R.id.action_copy_app_url) {
            Utils.copyToClipboard(this, getString(R.string.app_url));
            Toast.makeText(this, "copied to clipboard", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.action_help) {
            onNavigationDrawerItemSelected(9, "Help");
        } else if (id == R.id.action_share_app) {
            onNavigationDrawerItemSelected(7, "Share App");
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onFullscreen(boolean b) {

    }
}
