package ke.co.virtuaeye.celebrityapp.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

public class Provider extends ContentProvider {
    private static final String TAG = Provider.class.getSimpleName();
    private static final int SOURCES = 201;
    private static final int ARTICLES = 202;
    private static final int ARTICLE = 203;
    private static final int IMAGES_ALL = 204;
    private static final int IMAGES_OWNER = 205;


    static SQLiteQueryBuilder articlesQueryBuilder, articleQueryBuilder;

    static {
        articlesQueryBuilder = new SQLiteQueryBuilder();
        articlesQueryBuilder.setTables(Contract.ArticleEntry.TABLE_NAME + " LEFT JOIN " + Contract.ImageEntry.TABLE_NAME + " ON " +
                Contract.ArticleEntry.TABLE_NAME + "." + Contract.ArticleEntry.C_ID + "=" + Contract.ImageEntry.C_OWNER_ID);
    }

//    static {
//        articleQueryBuilder = new SQLiteQueryBuilder();
//        articleQueryBuilder.setTables(Contract.AgentEntry.TABLE_NAME + " , " + Contract.SourceEntry.TABLE_NAME + " LEFT JOIN " + Contract.ImageEntry.TABLE_NAME + " ON " +
//                Contract.SourceEntry.TABLE_NAME + "." + Contract.SourceEntry.C_ID + "=" + Contract.ImageEntry.C_OWNER_ID);
//    }


    private DbHelper dbHelper;

    public Provider() {
    }

    private static UriMatcher buildUriMatcher() {
        UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(Contract.CONTENT_AUTHORITY, Contract.PATH_ARTICLE, ARTICLES);
        sUriMatcher.addURI(Contract.CONTENT_AUTHORITY, Contract.PATH_SOURCE, SOURCES);
        sUriMatcher.addURI(Contract.CONTENT_AUTHORITY, Contract.PATH_IMAGE, IMAGES_ALL);
        sUriMatcher.addURI(Contract.CONTENT_AUTHORITY, Contract.PATH_ARTICLE + "/#", ARTICLE);
        sUriMatcher.addURI(Contract.CONTENT_AUTHORITY, Contract.PATH_IMAGE + "/#", IMAGES_OWNER);
        return sUriMatcher;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int result = -1;
        if (uri.equals(Contract.SourceEntry.CONTENT_URI))
            result = dbHelper.getWritableDatabase().delete(Contract.SourceEntry.TABLE_NAME, selection, selectionArgs);
        else if (uri.equals(Contract.ArticleEntry.CONTENT_URI))
            result = dbHelper.getWritableDatabase().delete(Contract.ArticleEntry.TABLE_NAME, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);

        return result;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Uri result = null;
        if (uri.equals(Contract.ArticleEntry.CONTENT_URI))
            result = Contract.ArticleEntry.buildContentUri(dbHelper.getWritableDatabase().insertWithOnConflict(Contract.ArticleEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE));

        else if (uri.equals(Contract.SourceEntry.CONTENT_URI))
            result = Contract.SourceEntry.buildContentUri(dbHelper.getWritableDatabase().insertWithOnConflict(Contract.SourceEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE));
        getContext().getContentResolver().notifyChange(uri, null);

        return result;
    }

    @Override
    public boolean onCreate() {
        dbHelper = new DbHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        int match = buildUriMatcher().match(uri);
        Log.i(TAG, "matched - " + match);

        Cursor retCursor;

        switch (match) {
            case SOURCES:
                retCursor = dbHelper.getReadableDatabase().query(
                        Contract.SourceEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case ARTICLES:
                retCursor = articlesQueryBuilder.query(dbHelper.getReadableDatabase(), projection, selection,
                        selectionArgs, Contract.ArticleEntry.TABLE_NAME + "." + Contract.ArticleEntry.C_ID, null, sortOrder);
                break;
//            case ARTICLE:
//                //the selection params will be extracted and applied to the relevant calling method
//                retCursor = articleQueryBuilder.query(dbHelper.getReadableDatabase(), projection,
//                        Contract.SourceEntry.TABLE_NAME + "." + Contract.SourceEntry.C_ID + "=" + ContentUris.parseId(uri)
//                                + " AND " + Contract.SourceEntry.C_AGENT_ID + "="
//                                + Contract.AgentEntry.TABLE_NAME + "." + Contract.AgentEntry.C_ID,
//                        selectionArgs, Contract.ImageEntry.C_URL, null, sortOrder);
//                break;
            case IMAGES_OWNER:
                retCursor = dbHelper.getReadableDatabase().query(Contract.ImageEntry.TABLE_NAME, projection,
                        Contract.ImageEntry.C_OWNER_ID + "=" + ContentUris.parseId(uri), selectionArgs, null, null, sortOrder);
                break;

            default:
                retCursor = null;
        }

        //Registers the content observer to recive any updates
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        int match = buildUriMatcher().match(uri);
        int result = -1;

        switch (match) {
            case ARTICLES:
                result = dbHelper.getWritableDatabase().update(Contract.ArticleEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case SOURCES:
                result = dbHelper.getWritableDatabase().update(Contract.SourceEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                result = -1;
                break;
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return result;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        int match = buildUriMatcher().match(uri);
        Log.i(TAG, "matched - " + match);
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        int returnCount = 0;
        long _id = -1;
        String table = null;
        switch (match) {
            case SOURCES:
                table = Contract.SourceEntry.TABLE_NAME;
                break;
            case ARTICLES:
                table = Contract.ArticleEntry.TABLE_NAME;
                break;
            case IMAGES_ALL:
                table = Contract.ImageEntry.TABLE_NAME;
                break;

        }

        if (table != null) {
            try {
                for (ContentValues value : values) {
                    _id = db.insertWithOnConflict(table, null, value, SQLiteDatabase.CONFLICT_REPLACE);
                    if (_id != -1)
                        returnCount++;
                }
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return returnCount;

    }
}
