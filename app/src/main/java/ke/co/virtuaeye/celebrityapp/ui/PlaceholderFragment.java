package ke.co.virtuaeye.celebrityapp.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ke.co.virtuaeye.celebrityapp.R;

/**
 * Created by Frederick on 8/24/2015.
 */
public class PlaceholderFragment extends Fragment {
    public static String ARG_TYPE = "type";
    public static int CONTACT_US = 1;
    public static int HELP = 2;
    public static int FEEDBACK = 3;
    public static int ABOUT = 4;


    public static PlaceholderFragment newInstance(int type){
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }
    public PlaceholderFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_placeholder, container, false);
        return root;
    }
}
