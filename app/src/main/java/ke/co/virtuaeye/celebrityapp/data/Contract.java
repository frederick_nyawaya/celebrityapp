package ke.co.virtuaeye.celebrityapp.data;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Frederick on 4/23/2015.
 */
public class Contract {

    // The "Content authority" is a name for the entire content provider, similar to the
    // relationship between a domain name and its website.  A convenient string to use for the
    // content authority is the package name for the app, which is guaranteed to be unique on the
    // device.
    public static final String CONTENT_AUTHORITY = "ke.co.virtuaeye.celebrityapp";

    // Use CONTENT_AUTHORITY to create the base of all URI's which apps will use to contact
    // the content provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_SOURCE = "source";
    public static final String PATH_ARTICLE = "article";
    public static final String PATH_IMAGE = "image";


    /* Inner class that defines the table contents of the sources table */
    public static final class SourceEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_SOURCE).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_SOURCE;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_SOURCE;

        // Table name
        public static final String TABLE_NAME = "sources";

        //Column names
        public static final String C_TITLE = "title";
        public static final String C_URL = "url";


        public static Uri buildContentUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    /* Inner class that defines the table contents of the articles table */
    public static final class ArticleEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_ARTICLE).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_ARTICLE;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_ARTICLE;

        // Table name
        public static final String TABLE_NAME = "articles";

        //Column names
        public static final String C_ID = "_ID";
        public static final String C_TITLE = "title";
        public static final String C_CONTENT = "content";
        public static final String C_DESCRIPTION = "description";
        public static final String C_URL = "url";
        public static final String C_AUTHOR = "author";
        public static final String C_SOURCE = "source";
        public static final String C_TIME = "time";


        public static Uri buildContentUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    /* Inner class that defines the table contents of the images table */
    public static final class ImageEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_IMAGE).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_IMAGE;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_IMAGE;

        // Table name
        public static final String TABLE_NAME = "images";

        //Column names
        public static final String C_ID = "_id";
        public static final String C_URL = "url";
        public static final String C_OWNER_ID = "ownerId";


        public static Uri buildImageUri(long ownerId) {
            return ContentUris.withAppendedId(CONTENT_URI, ownerId);
        }
    }


}
