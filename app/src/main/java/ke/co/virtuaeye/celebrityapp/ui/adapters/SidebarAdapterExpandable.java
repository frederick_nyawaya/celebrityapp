package ke.co.virtuaeye.celebrityapp.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ke.co.virtuaeye.celebrityapp.R;

/**
 * Created by Frederick on 8/25/2015.
 */
public class SidebarAdapterExpandable extends BaseExpandableListAdapter {
    private List<String> groups;
    private LayoutInflater inflater;
    private List<String> videoItems;
    private Context context;

    public SidebarAdapterExpandable(Context context) {
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
        this.notifyDataSetChanged();
    }

    public void setVideoItems(List<String> videoItems) {
        this.videoItems = videoItems;
        this.notifyDataSetChanged();
    }


    @Override
    public int getGroupCount() {
        return groups != null ? groups.size() : 0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return videoItems != null && groupPosition == 5 ? videoItems.size() : 0;
    }

    @Override
    public String getGroup(int groupPosition) {
        return groups != null ? groups.get(groupPosition) : "";
    }

    @Override
    public String getChild(int groupPosition, int childPosition) {
        return videoItems != null && groupPosition == 5 ? videoItems.get(childPosition) : null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_drawer, null);
            convertView.setTag(holder);
            holder.title = (TextView) convertView.findViewById(R.id.textView6);
            holder.indicator = (ImageView) convertView.findViewById(R.id.imageView6);

            //Utils.applyFonts(convertView, App.getRobotoSlabLight());

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (groupPosition == 5) {
            holder.indicator.setVisibility(View.VISIBLE);
                     holder.indicator.setBackground(isExpanded ?
                             context.getResources().getDrawable(R.drawable.ic_navigation_arrow_drop_up) :
                             context.getResources().getDrawable(R.drawable.ic_navigation_arrow_drop_down));
        }else{
            holder.indicator.setVisibility(View.GONE);

        }

        holder.title.setText(getGroup(groupPosition).toUpperCase());


        return convertView;
    }


    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (groupPosition != 5 ) return null;
        ViewHolder holder = new ViewHolder();

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_drawer_child, null);
            convertView.setTag(holder);
            holder.title = (TextView) convertView.findViewById(R.id.textView6);

            //Utils.applyFonts(convertView, App.getRobotoSlabLight());

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(getChild(groupPosition, childPosition).toUpperCase());


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private class ViewHolder {
        TextView title;
        ImageView indicator;
    }
}
