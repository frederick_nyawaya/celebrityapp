package ke.co.virtuaeye.celebrityapp.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import ke.co.virtuaeye.celebrityapp.R;

/**
 * Created by Frederick on 8/24/2015.
 */
public class SidebarAdapter extends BaseAdapter {
    LayoutInflater inflater;
    List<String> items;

    public SidebarAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void setItems(List<String> items) {
        this.items = items;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public String getItem(int position) {
        return items != null ? items.get(position) : "";
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();


        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_drawer, null);
            convertView.setTag(holder);
            holder.title = (TextView) convertView.findViewById(R.id.textView6);

            //Utils.applyFonts(convertView, App.getRobotoSlabLight());

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(getItem(position).toUpperCase());


        return convertView;
    }

    private class ViewHolder {
        TextView title;
    }
}
