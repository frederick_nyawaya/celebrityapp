package ke.co.virtuaeye.celebrityapp.gcm;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;


public class GcmRegService extends IntentService {
    private static final String TAG = GcmRegService.class.getSimpleName();
    private GoogleCloudMessaging gcm;

    // TODO: change to your own sender ID to Google Developers Console project number, as per instructions above
    private static final String SENDER_ID = "123061139749";


    public GcmRegService() {
        super("GCMRegService");
    }

    public static void register(Context context) {
        context.startService(new Intent(context, GcmRegService.class));
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String res = registerGcm();
            Log.i("ME", "gcm res - " + res);
        }
    }

    private String registerGcm() {
        String msg = "";
        try {
            if (gcm == null) {
                gcm = GoogleCloudMessaging.getInstance(this);
            }
            String regId = gcm.register(SENDER_ID);
            msg = "Device registered, registration ID=" + regId;

            // You should send the registration ID to your server over HTTP,
            // so it can use GCM/HTTP or CCS to send messages to your app.
            // The request to your server should be authenticated if your app
            // is using accounts.

            //regService.register(regId).execute();
            //CloudWork.sendPost("gcm_key="+regId + "&time"+new Date(System.currentTimeMillis()), CloudWork.GCM_URL_SEG);
            //persist reg status locally

            //Utils.setGcmRegistered(this, regId);


        } catch (IOException ex) {
            ex.printStackTrace();
            msg = "Error: " + ex.getMessage();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return msg;
    }


}
