package ke.co.virtuaeye.celebrityapp.ui;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ke.co.virtuaeye.celebrityapp.R;
import ke.co.virtuaeye.celebrityapp.sync.RestHandler;
import ke.co.virtuaeye.celebrityapp.utils.DefaultImageLoader;

/**
 * Created by Frederick on 8/24/2015.
 */
public class FacebookFragment extends Fragment implements LoaderManager.LoaderCallbacks<JSONArray> {

    private LayoutInflater inflater;
    private JSONArray data;
    private ListView list;
    private FacebookAapter adapter;
    private boolean dataReady;

    private ProgressBar progressBar;

    private String profilePic;

    RestHandler handler;

    public FacebookFragment() {
    }

    public static Fragment newInstance() {
        return new FacebookFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Drawable c = ((HomeActivity) getActivity()).toolbar.getBackground();
        c.setAlpha(Math.round(100 * 255));
        ((HomeActivity) getActivity()).toolbar.setBackground(c);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.inflater = inflater;
        handler = new RestHandler();
        handler.setCallback(new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                Log.v("MEE", graphResponse.toString());
            }
        });

        View parent = inflater.inflate(R.layout.list, container, false);
        progressBar = (ProgressBar) parent.findViewById(R.id.progressBar);


        list = (ListView) parent.findViewById(R.id.list);

        adapter = new FacebookAapter();
        list.setAdapter(adapter);

        return parent;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public Loader<JSONArray> onCreateLoader(int id, Bundle args) {
        progressBar.setVisibility(View.VISIBLE);
        list.setVisibility(View.GONE);

        return new AsyncTaskLoader<JSONArray>(getActivity()) {
            @Override
            protected void onStartLoading() {
                super.onStartLoading();
                if (dataReady) {
                    deliverResult(data);
                } else {
                    forceLoad();
                }
            }

            @Override
            public JSONArray loadInBackground() {
                try {
                    handler.getYoutubeVideos();
                    profilePic = handler.getProfilePic().getJSONObject().getJSONObject("picture").getJSONObject("data").getString("url");

                    Log.v("MEX", "pic - " + handler.getProfilePic().toString());


                    return handler.getContent().getJSONObject().getJSONArray("data");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<JSONArray> loader, JSONArray data) {
        adapter.setData(data);
        progressBar.setVisibility(View.GONE);
        list.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLoaderReset(Loader<JSONArray> loader) {

    }


    private class FacebookAapter extends BaseAdapter {
        JSONArray data;


        public void setData(JSONArray data) {
            this.data = data;
            this.notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return data != null ? data.length() : 0;
        }

        @Override
        public JSONObject getItem(int position) {
            try {
                return data.getJSONObject(position);
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = new ViewHolder();


            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_item_card_facebook, null);
                convertView.setTag(holder);
                holder.title = (TextView) convertView.findViewById(R.id.textView);
                holder.time = (TextView) convertView.findViewById(R.id.textView2);
                holder.content = (TextView) convertView.findViewById(R.id.textView3);

                holder.image = (ImageView) convertView.findViewById(R.id.imageView);
                holder.profilePic = (ImageView) convertView.findViewById(R.id.imageView7);

                //Utils.applyFonts(convertView, App.getRobotoSlabLight());

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            JSONObject item = getItem(position);


            try {
                holder.title.setText(item.getString("story"));
            } catch (JSONException e) {
                e.printStackTrace();
                holder.title.setText("Mohanlal");

            }

            try {
                holder.time.setText(formatDate(item.getString("created_time")));
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }

            try {
                holder.content.setText(item.getString("message"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                String imageUrl = item.getString("full_picture");
                if (imageUrl != null) {
                    holder.image.setVisibility(View.VISIBLE);
                    DefaultImageLoader.getInstance().loadImage(imageUrl, holder.image);
                } else {
                    holder.image.setVisibility(View.GONE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                DefaultImageLoader.getInstance().loadImage(profilePic, holder.profilePic);
            } catch (Exception e) {
                e.printStackTrace();
            }


            return convertView;
        }
    }

    private CharSequence formatDate(String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        Date dateParsed = sdf.parse(date);
        return DateUtils.getRelativeTimeSpanString(dateParsed.getTime());

    }

    private class ViewHolder {
        TextView title, time, content;
        ImageView image, profilePic;
    }
}

